
const btJogarJunKenPon = document.getElementById('bt__Play--JanKenPon')


btJogarJunKenPon.addEventListener('click', () => {

    const itemSelected = document.getElementById('game__select--JanKenPon')
    const imgJogador = document.getElementById('img__Jogador')
    const imgMaquina = document.getElementById('img__Maquina')
    const boxResultado = document.getElementById('vencedor__JanKenPon')
    boxResultado.classList.remove('hidden')

    const maquina = getRandomInt(0,3)
    
    addImg(itemSelected.selectedIndex, imgJogador)
    addImg(maquina, imgMaquina)
    
    winner(itemSelected.selectedIndex, maquina)


})

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

function addImg(itselect, player){
    
    if(itselect === 0){
        player.src = "https://cdn.pixabay.com/photo/2014/03/25/15/26/rock-paper-scissors-296854_960_720.png"
    }
    if(itselect === 1){
        player.src = "https://cdn.pixabay.com/photo/2014/03/25/15/26/rock-paper-scissors-296855_960_720.png"
    }
    if(itselect === 2){
        player.src = "https://cdn.pixabay.com/photo/2014/03/25/15/26/rock-paper-scissors-296853_960_720.png"
    }
}

function winner(player, machine){
    const vencedor = document.getElementById('vencedor')
    // EMPATE
    if(player === machine){
        vencedor.textContent = 'EMPATE'
    }
    // PLAYER WIN
    if(player === 0 && machine === 2){
        vencedor.textContent = 'VOCÊ GANHOU'
    }
    if(player === 1 && machine === 0){
        vencedor.textContent = 'VOCÊ GANHOU'
    }
    if(player === 2 && machine === 1){
        vencedor.textContent = 'VOCÊ GANHOU'
    }
    // MACHINE WIN
    if(machine === 0 && player === 2){
        vencedor.textContent = 'MÁQUINA GANHOU'
    }
    if(machine === 1 && player === 0){
        vencedor.textContent = 'MÁQUINA GANHOU'
    }
    if(machine === 2 && player === 1){
        vencedor.textContent = 'MÁQUINA GANHOU'
    }
}